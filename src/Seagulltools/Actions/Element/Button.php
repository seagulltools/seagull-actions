<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 2019-03-13
 * Time: 16:28
 */

namespace Seagulltools\Actions\Element;

use Seagulltools\Actions\Action;

class Button extends Action
{
    public $component = 'button-component';


    public function onClick($onclick)
    {
        return $this->withMeta([
            'onclick' => $onclick
        ]);
    }

}