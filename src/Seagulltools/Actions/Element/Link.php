<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 2019-03-13
 * Time: 15:19
 */

namespace Seagulltools\Actions\Element;

use Seagulltools\Actions\Action;

class Link extends Action
{
    public $component = 'link-component';
}