<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 2019-03-13
 * Time: 15:15
 */

namespace Seagulltools\Actions;


abstract class Action
{
    /**
     * The displayable name of the field.
     *
     * @var string
     */
    public $name;

    public $action;

    public $type;

    /**
     * The element's component.
     *
     * @var string
     */
    public $component;

    /**
     * The meta data for the element.
     *
     * @var array
     */
    public $meta = [];

    public function __construct($name, $type)
    {
        $this->name = $name;
        $this->type = $type;
    }

    /**
     * Create a new element.
     *
     * @return static
     */
    public static function make(...$arguments)
    {
        return new static(...$arguments);
    }

    public function action($action)
    {
        $this->action = $action;

        return $this;
    }

    public function mappings($mappings)
    {
        return $this->withMeta([ 'mappings' => $mappings  ?? [] ]);
    }

    public function function($function)
    {
        return $this->withMeta([ 'functions' => $function ?? '' ]);
    }

    public function forEachRow()
    {
        return $this->withMeta([ 'eachRows' => true ]);
    }

    /**
     * Set additional meta information for the element.
     *
     * @param  array  $meta
     * @return $this
     */
    public function withMeta(array $meta)
    {
        $this->meta = array_merge($this->meta, $meta);

        return $this;
    }

}